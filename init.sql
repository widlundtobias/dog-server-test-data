CREATE USER dogserver WITH PASSWORD 'dogsecret';
CREATE DATABASE dogdata;
GRANT ALL PRIVILEGES ON DATABASE dogdata TO dogserver;
